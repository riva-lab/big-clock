#include "microphone.h"
#include <fastIO.h>


Microphone::Microphone(uint8_t pin, uint8_t irqNo) {
  begin(pin, irqNo);
}

void Microphone::begin(uint8_t pin, uint8_t irqNo) {
  fastPinMode(_pin = pin, INPUT);
  _irqNo    = irqNo;
  _active   = 0;

  setParameters(3, 1); // default parameters: 3 pulses with 1s period
}

void Microphone::update() {
  static uint32_t timeStart;

  if (_irqDetected) {
    if (!_active) {
      uint16_t t = (uint16_t)(millis() - timeStart);
      if (_count && ((t < _minDelay) || (t > _maxDelay)))
        _count = 0;
    }

    timeStart       = millis();
    _active         = 1;
    _irqDetected    = 0;
  }

  if (_active && ((uint16_t)(millis() - timeStart) > MIC_DEBOUNCE_DELAY)) {
    _active         = 0;
    timeStart       = millis();
    if (_count < _pulsesMax) _count++;
  }
}

void Microphone::setParameters(uint8_t pulses, uint8_t period_seconds) {
  _pulsesMax    = pulses;
  _minDelay     = (uint16_t)((100 - MIC_INTERPULSE_ACCURACY) * 10 * period_seconds);
  _maxDelay     = (uint16_t)((100 + MIC_INTERPULSE_ACCURACY) * 10 * period_seconds);
}

uint8_t Microphone::activated() {
  uint8_t tmp = (_count >= _pulsesMax);
  if (tmp) _count = 0;
  return tmp;
  //    return _micActive;
}

void Microphone::setIrqHandler(irqHandler_t func) {
  attachInterrupt(_irqNo, func, RISING);
}

void Microphone::setIrqFlag() {
  _irqDetected = 1;
}
