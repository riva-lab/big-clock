#ifndef MICROPHONE_H
#define MICROPHONE_H

#include <stdint.h>


#define MIC_DEBOUNCE_DELAY          100     // [мс]
#define MIC_INTERPULSE_ACCURACY     60      // [%]


typedef (*irqHandler_t)();


class Microphone {

  public:

    Microphone(uint8_t pin, uint8_t irqNo);

    void    begin(uint8_t pin, uint8_t irqNo);
    void    update();
    void    setParameters(uint8_t pulses, uint8_t period_seconds);
    uint8_t activated();

    void    setIrqHandler(irqHandler_t func);
    void    setIrqFlag();


  private:

    uint8_t     _pin;
    uint8_t     _irqNo;

    uint16_t    _minDelay;
    uint16_t    _maxDelay;
    uint8_t     _pulsesMax;

    uint8_t     _irqDetected;
    uint8_t     _active;
    uint8_t     _count;
};


#endif /* MICROPHONE_H */
