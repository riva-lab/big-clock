#include "menu.h"
#include "common.h"
#include "timeout.h"


void Menu::begin(uint8_t itemMin, uint8_t itemMax) {
  _item = _min = itemMin;
  _max  = itemMax;
  _onChange = 0;
  _onEnter  = 0;
}

void Menu::show() {

  uint8_t change = MENU_CHNG_ENTER;

  Timeout timeoutHome(0);
  Timeout timeoutHold(0);

  btn.reset();

  while (1) {

    if (timeoutHold.expired())
      switch (btn.getHolding()) {   // обработка зажатых кнопок в меню

        case BUTTON_DN:             // выбор следующего п.м.
          timeoutHold.begin(MENU_TIME_SLIDING);
          btn.click(BUTTON_DN);
          break;

        case BUTTON_UP:             // выбор предыдущего п.м.
          timeoutHold.begin(MENU_TIME_SLIDING);
          btn.click(BUTTON_UP);
          break;
      }

    switch (btn.getClicked()) {     // обработка коротких нажатий кнопок в меню

      case BUTTON_DN:               // выбор следующего п.м.
        if (_item == _max) _item = _min;
        else _item++;
        change = MENU_CHNG_LEFT;
        break;

      case BUTTON_UP:               // выбор предыдущего п.м.
        if (_item == _min) _item = _max;
        else _item--;
        change = MENU_CHNG_RIGHT;
        break;

      case BUTTON_MD:               // выбор п.м.
        if (_onEnter) _onEnter(_item);
        change = MENU_CHNG_ENTER;
        break;
    }

    switch (btn.getPressed()) {     // обработка длинных нажатий кнопок в меню

      case BUTTON_MD:               // выход из меню
        return;
    }

    if (change) {
      timeoutHome.begin(MENU_TIMEOUT_HOME);
      if (_onChange) _onChange(_item, change);
      change = MENU_CHNG_NO;
    }

    if (timeoutHome.expired()) {    // выход из меню по таймауту неактивности
      return;
    }
  }
}

uint8_t Menu::item() {
  return _item;
}

void Menu::onChange(chngHandler_t func) {
  _onChange = func;
}
void Menu::onEnter(menuHandler_t func) {
  _onEnter = func;
}
