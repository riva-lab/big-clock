#include "sett_item.h"
#include <avr/pgmspace.h>

#if defined(ARDUINO) && (ARDUINO >= 100)
#include "Arduino.h"
#else
#include "WProgram.h"
#include <pins_arduino.h>
#endif


dialog_t        dialogOut;
settingItem_t   settingItemOut;
char            strOut[PARAM_TXT_SIZE];


/*********************************************************************/
/*                      Стандартные (из ОЗУ)                         */
/*********************************************************************/

dialog_t * getDlgStruct(const dialog_t *dialog) {
  memcpy(&dialogOut, dialog, sizeof(dialog_t));
  return &dialogOut;
}

settingItem_t * getItemStruct(const settingItem_t *siArr) {
  memcpy(&settingItemOut, siArr, sizeof(settingItem_t));
  return &settingItemOut;
}

char * getString(const char * strArr, uint8_t index) {
  memcpy(strOut, strArr + index * PARAM_TXT_SIZE, PARAM_TXT_SIZE);
  return strOut;
}

/*********************************************************************/
/*                        Из Flash (PROGMEM)                         */
/*********************************************************************/

dialog_t * getDlgStructPGM(const dialog_t *dialog PROGMEM) {
  memcpy_P(&dialogOut, dialog, sizeof(dialog_t));
  return &dialogOut;
}

settingItem_t * getItemStructPGM(const settingItem_t *siArr PROGMEM) {
  memcpy_P(&settingItemOut, siArr, sizeof(settingItem_t));
  return &settingItemOut;
}

char * getStringPGM(const char * strArr PROGMEM, uint8_t index) {
  memcpy_P(strOut, strArr + index * PARAM_TXT_SIZE, PARAM_TXT_SIZE);
  return strOut;
}
