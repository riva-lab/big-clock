#ifndef SHOW_H
#define SHOW_H

#include <stdint.h>


void    showTime(uint8_t h, uint8_t m, uint8_t s, uint8_t w);   // вывод времени на индикаторы панели с форматированием по текущему стилю
void    showDate(uint8_t y, uint8_t m, uint8_t d, uint8_t w);   // вывод даты на индикаторы панели с форматированием по текущему стилю
void    showWeekDay(uint8_t d);                                 // вывод дня недели на индикаторы панели

void    showClock();            // вывод часов на индикаторы панели
void    showSetModeAuto();
void    showSetModeNext();
void    showSetModePrev();
uint8_t showIsModeChange();

#endif /* SHOW_H */
