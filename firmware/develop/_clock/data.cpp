#include "data.h"
#include "common.h"
#include <EEPROM.h>

#if defined(ARDUINO) && (ARDUINO >= 100)
#include "Arduino.h"
#else
#include "WProgram.h"
#include <pins_arduino.h>
#endif


void ClockData::setEepromAddress(uint16_t start_address) {
  _startAddress = start_address;
}

uint8_t ClockData::dataLength() {
  return EEPROM_PARAMETERS_SIZE;
}

void ClockData::begin() {
  for (uint8_t i = 0; i < EEPROM_PARAMETERS_SIZE; i++)
    _data.bytes[i] = EEPROM.read(_startAddress + i);

  utc.setZone(_data.timeZone);
  utc.setSummerTime(_data.summerTimeMode == SUMMERTIME_MODE_ON);
  utc.setSummerTimeAuto(_data.summerTimeMode == SUMMERTIME_MODE_AUTO);
  utc.setSummerTimeDelta(_data.summerTimeMinutes);

  ssp.setBrightness(BRIGHTNESS(_data.brightLevel));
}

void ClockData::update() {
  for (uint8_t i = 0; i < EEPROM_PARAMETERS_SIZE; i++)
    if (_data.bytes[i] != EEPROM.read(_startAddress + i)) {
      EEPROM.write(_startAddress + i, _data.bytes[i]);
      //      Serial.print(F("eeprom write byte #"));
      //      Serial.println(i);
    }
}

void ClockData::setTime(uint8_t h, uint8_t m, uint8_t s = 0) {
  utc.updateUTC();
  utc.setTime(h, m, s);
  utc.update();
}

void ClockData::getTime(uint8_t *h, uint8_t *m, uint8_t *s) {
  utc.updateUTC();
  utc.getTime(h, m, s);
  utc.update();
}

void ClockData::setDate(uint8_t y, uint8_t m, uint8_t d) {
  utc.updateUTC();
  utc.setDate(2000 + y, m, d);
  utc.update();
}

void ClockData::getDate(uint8_t *y, uint8_t *m, uint8_t *d) {
  uint16_t year;
  utc.updateUTC();
  utc.getDate(&year, m, d);
  utc.update();
  *y = year - 2000;
}

uint8_t ClockData::getDayOfWeek() {
  utc.update();
  return utc.getDayOfWeek();
}

void ClockData::setTimeZone(int8_t h, int8_t m) {
  utc.setZone(_data.timeZone = h * 60 + m);
}

void ClockData::getTimeZone(int8_t *h, int8_t *m) {
  *h = _data.timeZone / 60;
  *m = _data.timeZone % 60;
}

void ClockData::setSoundIndex(uint8_t alarmNo, uint8_t index) {
  _data.alarmSound[alarmNo - 1] = index;
}

uint8_t ClockData::getSoundIndex(uint8_t alarmNo) {
  return _data.alarmSound[alarmNo - 1];
}

void ClockData::setNightTimeBegin(uint8_t h, uint8_t m) {
  _data.nightStart = h * 60 + m;
}

void ClockData::getNightTimeBegin(uint8_t *h, uint8_t *m) {
  *h = _data.nightStart / 60;
  *m = _data.nightStart % 60;
}

uint16_t ClockData::getNightTimeBegin() {
  return _data.nightStart;
}

void ClockData::setNightTimeEnd(uint8_t h, uint8_t m) {
  _data.nightFinish = h * 60 + m;
}

void ClockData::getNightTimeEnd(uint8_t *h, uint8_t *m) {
  *h = _data.nightFinish / 60;
  *m = _data.nightFinish % 60;
}

uint16_t ClockData::getNightTimeEnd() {
  return _data.nightFinish;
}

uint8_t ClockData::isNightTime(uint8_t h, uint8_t m) {
  if (_data.nightStart != _data.nightFinish) {
    uint16_t tc = h * 60 + m;

    return (tc <= _data.nightFinish) ||
           (tc >= _data.nightStart);
  } else
    return 0;
}

void ClockData::setBrightLevel(uint8_t level) {
  _data.brightLevel = level;
}

uint8_t ClockData::getBrightLevel() {
  return _data.brightLevel;
}

void ClockData::setBell(uint8_t enable, uint8_t minutes) {
  _data.bellEnable = enable;
  _data.bellMinutes = minutes;
}

void ClockData::getBell(uint8_t *enable, uint8_t *minutes) {
  *enable = _data.bellEnable;
  *minutes = _data.bellMinutes;
}

void ClockData::setSummerTime(uint8_t mode, uint8_t minutes) {
  _data.summerTimeMode = mode;
  _data.summerTimeMinutes = (int8_t)minutes;

  utc.setSummerTime(_data.summerTimeMode == SUMMERTIME_MODE_ON);
  utc.setSummerTimeAuto(_data.summerTimeMode == SUMMERTIME_MODE_AUTO);
  utc.setSummerTimeDelta(_data.summerTimeMinutes);
}

void ClockData::getSummerTime(uint8_t *mode, uint8_t *minutes) {
  *mode = _data.summerTimeMode;
  *minutes = _data.summerTimeMinutes;
}

void ClockData::setBeep(uint8_t enable) {
  _data.beepEnable = enable;
}

uint8_t ClockData::getBeep() {
  return _data.beepEnable;
}

void ClockData::setStyleTime(uint8_t index) {
  _data.styleTime = index;
}

uint8_t ClockData::getStyleTime() {
  return _data.styleTime;
}

void ClockData::setStyleDate(uint8_t index) {
  _data.styleDate = index;
}

uint8_t ClockData::getStyleDate() {
  return _data.styleDate;
}

void ClockData::setWeekDayView(uint8_t enable) {
  _data.weekDayView = enable;
}

uint8_t ClockData::getWeekDayView() {
  return _data.weekDayView;
}

void ClockData::setViewEffect(uint8_t index) {
  _data.viewEffect = index;
}

uint8_t ClockData::getViewEffect() {
  return _data.viewEffect;
}
