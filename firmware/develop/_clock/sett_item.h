#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <stdint.h>
#include <avr/pgmspace.h>


#define PARAM_DLG_MAX       3   // максимальное количество параметров в диалоге

#define PARAM_TYPE_EMPTY    0   // тип параметра - нет (не выводится)
#define PARAM_TYPE_UINT     1   // тип параметра - число беззнаковое uint8_t
#define PARAM_TYPE_INT      2   // тип параметра - число со знаком int8_t
#define PARAM_TYPE_LABEL    3   // тип параметра - текстовая метка (нередактируемый параметр)
#define PARAM_TYPE_STR      4   // тип параметра - текст из массива по индексу
#define PARAM_TYPE_FLAGS    5   // тип параметра - флаги

#define PARAM_DELIM_NO      0   // разделитель параметра - нет
#define PARAM_DELIM_DOT1    1   // разделитель параметра - 1 точка после
#define PARAM_DELIM_DOT2    2   // разделитель параметра - 2 точки после

#define PARAM_ITEM_SIZE     8   // длина строки с названием параметра
#define PARAM_TXT_SIZE      12  // длина текстового параметра


/*********************************************************************/
/*               Описание структур для пунктов настроек              */
/*********************************************************************/

// описание структуры диалога
typedef struct {
  struct {
    uint8_t     type;                   // тип
    char        label[PARAM_TXT_SIZE];  // строка (для текстовой метки)
    char      * str;                    // указатель на начало массива строк
    uint8_t     min;                    // мин. значение
    uint8_t     max;                    // макс. значение
    uint8_t     pos;                    // позиция вывода параметра
    uint8_t     length;                 // ограничение длины вывода параметра
    uint8_t     delim;                  // разделитель
  } param[PARAM_DLG_MAX];               // массив параметров

} dialog_t;

// структура пункта настройки
typedef struct {
  char          caption[PARAM_ITEM_SIZE];   // отображаемое название пункта
  dialog_t    * dialog;                     // указатель на диалог
} settingItem_t;


// доступ к структурам, хранящимся в ОЗУ (стандартно)
dialog_t      * getDlgStruct        (const dialog_t       * dialog);
settingItem_t * getItemStruct       (const settingItem_t  * siArr);
char          * getString           (const char * strArr, uint8_t index);


// доступ к структурам, хранящимся в Flash (использование PROGMEM)
dialog_t      * getDlgStructPGM     (const dialog_t       * dialog  PROGMEM);
settingItem_t * getItemStructPGM    (const settingItem_t  * siArr   PROGMEM);
char          * getStringPGM        (const char * strArr PROGMEM, uint8_t index);


#endif /* PARAMETERS_H */
