#ifndef SETTINGS_H
#define SETTINGS_H

#include <stdint.h>


// временные параметры, мс
#define TIME_KEY_REPEAT     100     // интервал между повтором при зажатой кнопке
#define TIMEOUT_BLINK       1000    // время, по истечению которого начинает мигать текущий параметр
#define TIMEOUT_CAPTION     2000    // время задержки надписи на дисплее
#define TIMEOUT_EXIT        45000   // таймаут выхода из опции по бездействию
#define TIMEOUT_PLAY        1000    // таймаут до начала проигрывания мелодии

void settings();                    // УСТАНОВКИ - [SEtUP]
void settingsOfAlarms();            // УСТАНОВКИ БУДИЛЬНИКОВ - [ALArnnS]

#endif /* SETTINGS_H */
