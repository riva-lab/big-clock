#ifndef COMMON_H
#define COMMON_H


#if defined(ARDUINO) && (ARDUINO >= 100)
#include "Arduino.h"
#else
#include "WProgram.h"
#include <pins_arduino.h>
#endif


#include "buttons.h"

extern uint8_t buttonsPin[];
extern Buttons btn;



#include "SevenSegmentPanel.h"

extern SevenSegmentPanel ssp;



#include "embedDateTime.h"
#include "Alarms.h"

#define ALARMS_DATA_ADDRESS 64

extern embedDateTime       utc;
extern Alarms              alarm;
extern t_datetime_struct   datetime;
extern int8_t              zone, zonem, nightTime, alarmWork;
extern uint8_t             summer_t;



#include "data.h"

#define CLOCK_DATA_ADDRESS  0

extern ClockData clockData;



#include "SimpleRTTTL.h"
#include "userMelodies.h"

extern SimpleRTTTL player;



void commonInit();                      // общая инициализация
void bellsCheck(uint8_t h, uint8_t m);  // проверка и действие курантов


#define BRIGHTNESS_AVG_LEVEL    16      // уровень усреднения датчика света, больше - плавнее - медленнее изменяется яркость
#define BRIGHTNESS_MIN          5       // мин. уровень яркости панели
#define BRIGHTNESS_MAX          255     // макс. уровень яркости панели
#define R_PHR_DARK              300     // [кОм] сопротивление ФР темновое
#define R_PHR_LIGHT             10      // [кОм] сопротивление засвеченного ФР
#define R_PHR_PULLUP            100     // [кОм] сопротивление подтягивающего резистора
#define ADC_PHR_MIN             (uint16_t)(1023UL * R_PHR_LIGHT / (R_PHR_PULLUP + R_PHR_LIGHT))
#define ADC_PHR_MAX             (uint16_t)(1023UL * R_PHR_DARK / (R_PHR_PULLUP + R_PHR_DARK))

void autoBrightnessCheck();             // авторегулирование яркости панели

#endif /* COMMON_H */
