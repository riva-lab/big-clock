#ifndef DATA_H
#define DATA_H

#include <stdint.h>
#include "Alarms.h"


#define EEPROM_PARAMETERS_SIZE      32

// коды режима летнего времени, параметр mode для setSummerTime() / getSummerTime()
#define SUMMERTIME_MODE_OFF         0   // летнее время отключено
#define SUMMERTIME_MODE_ON          1   // летнее время включено всегда
#define SUMMERTIME_MODE_AUTO        2   // летнее время устанавливается автоматически

// яркость индикаторов панели
#define BRIGHT_AUTO                 0   // яркость будет устанавливаться по датчику освещенности
#define BRIGHT_MIN                  1   // мин. уровень
#define BRIGHT_MAX                  32  // макс. уровень
#define BRIGHTNESS(x)               ((uint16_t)0xFF * x / BRIGHT_MAX)

// коды эффектов для setViewEffect() / getViewEffect()
#define VIEW_EFFECT_OFF             0   // эффекты отключены
#define VIEW_EFFECT_DOTS            1   // только мигающие точки / разделители
#define VIEW_EFFECT_ALL             2   // включены все эффекты

class ClockData {

  public:

    void        setEepromAddress(uint16_t start_address);
    uint8_t     dataLength();

    void        begin();    // считывание настроек
    void        update();   // сохранение настроек

    void        setTime(uint8_t  h, uint8_t  m, uint8_t  s = 0);
    void        getTime(uint8_t *h, uint8_t *m, uint8_t *s);

    void        setDate(uint8_t  y, uint8_t  m, uint8_t  d);
    void        getDate(uint8_t *y, uint8_t *m, uint8_t *d);

    uint8_t     getDayOfWeek();

    void        setTimeZone(int8_t  h, int8_t  m);
    void        getTimeZone(int8_t *h, int8_t *m);

    void        setSoundIndex(uint8_t alarmNo, uint8_t index);
    uint8_t     getSoundIndex(uint8_t alarmNo);

    void        setNightTimeBegin(uint8_t  h, uint8_t  m);
    void        getNightTimeBegin(uint8_t *h, uint8_t *m);
    uint16_t    getNightTimeBegin();

    void        setNightTimeEnd(uint8_t  h, uint8_t  m);
    void        getNightTimeEnd(uint8_t *h, uint8_t *m);
    uint16_t    getNightTimeEnd();
    uint8_t     isNightTime(uint8_t h, uint8_t m);

    void        setBrightLevel(uint8_t level);
    uint8_t     getBrightLevel();

    void        setBell(uint8_t  enable, uint8_t  minutes);
    void        getBell(uint8_t *enable, uint8_t *minutes);

    void        setSummerTime(uint8_t  mode, uint8_t  minutes);
    void        getSummerTime(uint8_t *mode, uint8_t *minutes);

    void        setBeep(uint8_t enable);
    uint8_t     getBeep();

    void        setStyleTime(uint8_t index);
    uint8_t     getStyleTime();

    void        setStyleDate(uint8_t index);
    uint8_t     getStyleDate();

    void        setWeekDayView(uint8_t enable);
    uint8_t     getWeekDayView();

    void        setViewEffect(uint8_t index);
    uint8_t     getViewEffect();


  private:

    uint16_t _startAddress;

    union {
      uint8_t bytes[EEPROM_PARAMETERS_SIZE];

      struct {
        int16_t timeZone;

        uint8_t summerTimeMode;
        int8_t  summerTimeMinutes;

        uint16_t nightStart;
        uint16_t nightFinish;

        uint8_t brightLevel;

        uint8_t bellEnable;
        uint8_t bellMinutes;

        uint8_t beepEnable;

        uint8_t styleTime;
        uint8_t styleDate;
        uint8_t weekDayView;

        uint8_t viewEffect;
        uint8_t alarmSound[ALARMS_MAX];
      };

    } _data;
};

#endif /* DATA_H */
