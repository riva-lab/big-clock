#ifndef MENU_H
#define MENU_H

#include <stdint.h>


// временные параметры, мс
#define MENU_TIME_SLIDING   800     // интервал между сменами меню при зажатой кнопке
#define MENU_TIMEOUT_HOME   60000   // таймаут выхода из меню настроек по бездействию

// код события в меню, для chngHandler_t
#define MENU_CHNG_NO        0       // нет событий
#define MENU_CHNG_LEFT      1       // перемещение влево
#define MENU_CHNG_RIGHT     2       // перемещение вправо
#define MENU_CHNG_ENTER     3       // выбор / ввод
#define MENU_CHNG_EDIT      4       // редактирование


typedef (*menuHandler_t)(uint8_t item);
typedef (*chngHandler_t)(uint8_t item, uint8_t chng);

class Menu {

  public:

    void    begin(uint8_t itemMin, uint8_t itemMax);
    void    show();
    uint8_t item();

    void    onChange(chngHandler_t func);
    void    onEnter(menuHandler_t func);


  private:

    uint8_t _min;
    uint8_t _max;
    uint8_t _item;

    chngHandler_t _onChange;
    menuHandler_t _onEnter;
};

#endif /* MENU_H */
