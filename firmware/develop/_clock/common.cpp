#include "common.h"
#include "connections.h"
#include <MsTimer2.h>


uint8_t             buttonsPin[] = {PIN_BTN_UP, PIN_BTN_MD, PIN_BTN_DN};
Buttons             btn;


SevenSegmentPanel   ssp(PIN_CLK, PIN_DATA, PIN_STR, PIN_PWM);


embedDateTime       utc;
Alarms              alarm(&utc);
t_datetime_struct   datetime;
int8_t              zone, zonem, nightTime = 1, alarmWork = 0;
uint8_t             summer_t;


ClockData           clockData;

SimpleRTTTL         player;


// обработчик прерывания таймера 2 (период 1 мс)
void sysTimer1ms() {
  btn.update();     // сканирование и обработка кнопок
  player.update();  // обновление проигрывателя RTTTL
  ssp.autoUpdate(); // обновление индикаторов панели

  // подаем звук нажатия на кнопки
  uint8_t action = btn.getAction();
  if (clockData.getBeep() && !nightTime && !alarmWork) {
    switch (action) {

      case BUTTON_ACTION_CLICK:
        player.load(sound_click);
        player.play();
        break;

      case BUTTON_ACTION_PRESS:
        player.load(sound_press);
        player.play();
        break;
    }
  }
}

// общая инициализация
void commonInit() {
  fastPinMode(13, OUTPUT);
  fastPinMode(PIN_PHOTO_R, INPUT);

  // кнопки
  btn.begin();

  // семисегментная панель
  ssp.setDirection(1);          // задаем направление вывода данных в регистры
  ssp.setDigitRotation(2, 1);   // 2-й индикатор перевернут (отсчет от 0)
  ssp.setDigitRotation(4, 1);   // 4-й индикатор перевернут (отсчет от 0)
  ssp.setBlinkPeriod(700);
  ssp.clear();

  // часы RTC
  utc.rtc.begin();
  utc.begin();

  // будильники
  alarm.begin();
  alarm.restore(ALARMS_DATA_ADDRESS);
  for (uint8_t i = 0; i < ALARMS_MAX; i++)
    alarm.setDuration(i, 1);

  // настройки часов
  clockData.setEepromAddress(CLOCK_DATA_ADDRESS);
  clockData.begin();

  // таймер 2 (период 1 мс)
  MsTimer2::set(1, sysTimer1ms);
  MsTimer2::start();
}

// проверка и действие курантов
void bellsCheck(uint8_t h, uint8_t m) {
  uint8_t bellsEn, bellsMin;

  clockData.getBell(&bellsEn, &bellsMin);

  if (bellsEn && !nightTime && !alarmWork) {
    uint16_t tc = h * 60 + m;
    static uint16_t tcLast = tc;
    if ((tc != tcLast) && !(tc % bellsMin)) {
      tcLast = tc;

      player.load((tc % 60) ? bells_normal : bells_hour);
      player.play();
    }
  }
}

// авторегулирование яркости панели
void autoBrightnessCheck() {
  static uint32_t   summ = 0;
  uint16_t          avg;
  static uint8_t    levelPrevious;
  uint8_t           level;

  avg   = summ / BRIGHTNESS_AVG_LEVEL;
  (summ > avg) ? summ -= avg : summ = 0;
  summ += analogRead(PIN_PHOTO_R);
  avg   = summ / BRIGHTNESS_AVG_LEVEL;

  if      (avg < ADC_PHR_MIN) level = BRIGHTNESS_MAX;
  else if (avg < ADC_PHR_MAX) level = map(avg, ADC_PHR_MIN, ADC_PHR_MAX, BRIGHTNESS_MAX, BRIGHTNESS_MIN);
  else                        level = BRIGHTNESS_MIN;

  if (level != levelPrevious)
    ssp.setBrightness(level);
  levelPrevious = level;
}
