#ifndef BUTTONS_H
#define BUTTONS_H

#include <stdint.h>


#define BUTTON_CLICK_TIME       30      // [мс]
#define BUTTON_PRESS_TIME       700     // [мс]

#define BUTTONS_COUNT           3       // макс. 8 кнопок
#define BUTTON_NONE             0
#define BUTTON_UP               0x04    // код кнопки ВВЕРХ
#define BUTTON_MD               0x02    // код кнопки ВВОД
#define BUTTON_DN               0x01    // код кнопки ВНИЗ

#define BUTTON_ACTION_NO        0       // событие клаватуры: нет
#define BUTTON_ACTION_CLICK     1       // событие клаватуры: есть короткое нажатие 1+ кнопок
#define BUTTON_ACTION_PRESS     2       // событие клаватуры: есть длинное нажатие/удерживание 1+ кнопок


class Buttons {

  public:

    void    begin();                    // инициализация
    void    reset();                    // сброс
    void    update();                   // обработка состояния кнопок, вызывать периодически

    void    click(uint8_t buttonCode);  // эмуляция короткого нажатия
    void    press(uint8_t buttonCode);  // эмуляция длинного нажатия
    uint8_t getClicked();               // получение кода коротко нажатой кнопки
    uint8_t getPressed();               // получение кода длинно нажатой кнопки
    uint8_t getHolding();               // получение кода удерживаемой кнопки
    uint8_t getAction();                // получение статуса клавиатуры

  private:

    union {
      struct {
        uint8_t  status;
        uint16_t time;
      };

      struct {
        unsigned activated      : 1;
        unsigned activatedPrev  : 1;
        unsigned click          : 1;
        unsigned press          : 1;
        unsigned hold           : 1;
        unsigned                : 1;
        unsigned clickFlag      : 1;
        unsigned pressFlag      : 1;
      };
    } button[BUTTONS_COUNT];

    uint8_t _actionCode;
};


#endif /* BUTTONS_H */
