#include "settings.h"
#include "common.h"
#include "timeout.h"
#include "sett_list.h"
#include "menu.h"


#define DLG_RESULT_OK       0
#define DLG_RESULT_ERROR    254
#define DLG_RESULT_CANCEL   255

#define CHNG_NO             0
#define CHNG_LEFT           1
#define CHNG_RIGHT          2
#define CHNG_ENTER          3
#define CHNG_EDIT           4


settingItem_t *settItem;
settingItem_t *settAlarmItem;
uint8_t _enBrDyn = 0;
uint8_t _brLevel = 0;
uint8_t _soundPP = 0;


void brightDynamicEnable(uint8_t enable) {
  _enBrDyn = enable;
}

void brightDynamicUpdate(uint8_t level) {
  if (_enBrDyn)
    if (level)  ssp.setBrightness(BRIGHTNESS(level));
    else        autoBrightnessCheck();
}

void brightLevelArchive() {
  _brLevel = clockData.getBrightLevel();
}

void brightLevelRestore() {
  //  ssp.setBrightness(BRIGHTNESS(_brLevel));
  if (_brLevel) ssp.setBrightness(BRIGHTNESS(_brLevel));
  else          autoBrightnessCheck();
}

void soundPreplay(uint8_t enable) {
  if (_soundPP && !enable) player.stop();
  _soundPP = enable;
}

void soundPreplayUpdate(uint8_t index) {
  if (_soundPP) {
    player.load(GET_MELODY_FROM_LIST(index - 1));
    player.play();
  }
}

// диалог изменения параметров (макс.кол-во = PARAM_DLG_MAX)
uint8_t dialog(dialog_t * dialog, uint8_t value[]) {
  uint8_t parCurrent = 0;
  uint8_t change = CHNG_ENTER;

  Timeout timeoutHold(0);
  Timeout timeoutExit;
  Timeout timeoutBlink(0);
  Timeout timeoutPlay(TIMEOUT_PLAY);

  if (dialog == NULL) return DLG_RESULT_ERROR;
  dialog_t * dlg = getDlgStructPGM(dialog);
  uint8_t flagCurrent = dlg->param[0].min;

  btn.reset();
  ssp.setEffect(EFFECT_NONE);

  for (uint8_t i = 0; i < PARAM_DLG_MAX; i++) {
    uint8_t val = value[i];

    switch (dlg->param[parCurrent].type) {

      case PARAM_TYPE_UINT:
      case PARAM_TYPE_STR:
        if (val > dlg->param[i].max) val = dlg->param[i].max;
        if (val < dlg->param[i].min) val = dlg->param[i].min;
        break;

      case PARAM_TYPE_INT:
        if ((int8_t)val > (int8_t)dlg->param[i].max) val = dlg->param[i].max;
        if ((int8_t)val < (int8_t)dlg->param[i].min) val = dlg->param[i].min;
        break;
    }
    value[i] = val;
  }

  while (parCurrent < PARAM_DLG_MAX) {

    if (timeoutHold.expired())
      switch (btn.getHolding()) { // обработка зажатых кнопок

        case BUTTON_DN:
          timeoutHold.begin(TIME_KEY_REPEAT);
          btn.click(BUTTON_DN);
          break;

        case BUTTON_UP:
          timeoutHold.begin(TIME_KEY_REPEAT);
          btn.click(BUTTON_UP);
          break;
      }

    uint8_t click = btn.getClicked();
    if (click) change = CHNG_EDIT;

    switch (click) {    // обработка коротких нажатий кнопок

      case BUTTON_DN:   // изменение параметра
        timeoutPlay.begin(TIMEOUT_PLAY);
        switch (dlg->param[parCurrent].type) {

          case PARAM_TYPE_UINT:
          case PARAM_TYPE_INT:
            if (value[parCurrent] == dlg->param[parCurrent].min)
              value[parCurrent] = dlg->param[parCurrent].max;
            else
              value[parCurrent]--;
            break;

          case PARAM_TYPE_STR:
            if (value[parCurrent] == dlg->param[parCurrent].max)
              value[parCurrent] = dlg->param[parCurrent].min;
            else
              value[parCurrent]++;
            break;

          case PARAM_TYPE_FLAGS:
            uint8_t mask = 1 << flagCurrent;
            if (value[parCurrent] & mask)
              value[parCurrent] &= ~mask;
            else
              value[parCurrent] |= mask;
            break;
        }
        break;

      case BUTTON_UP:   // изменение параметра
        timeoutPlay.begin(TIMEOUT_PLAY);
        switch (dlg->param[parCurrent].type) {

          case PARAM_TYPE_UINT:
          case PARAM_TYPE_INT:
            if (value[parCurrent] == dlg->param[parCurrent].max)
              value[parCurrent] = dlg->param[parCurrent].min;
            else
              value[parCurrent]++;
            break;

          case PARAM_TYPE_STR:
            if (value[parCurrent] == dlg->param[parCurrent].min)
              value[parCurrent] = dlg->param[parCurrent].max;
            else
              value[parCurrent]--;
            break;

          case PARAM_TYPE_FLAGS:
            uint8_t mask = 1 << flagCurrent;
            if (value[parCurrent] & mask)
              value[parCurrent] &= ~mask;
            else
              value[parCurrent] |= mask;
            break;
        }
        break;

      case BUTTON_MD:   // переход к следующему
        if ((dlg->param[parCurrent].type == PARAM_TYPE_FLAGS) &&
            (flagCurrent < dlg->param[parCurrent].max))
          flagCurrent++;
        else {
          parCurrent++;
          flagCurrent = dlg->param[parCurrent].min;
        }
        change = CHNG_ENTER;
        break;
    }

    switch (btn.getPressed()) {
      case BUTTON_MD:   // длинное нажатие - отмена диалога
        ssp.blinkOff();
        return DLG_RESULT_CANCEL;
    }

    if (change != CHNG_NO) {
      ssp.clear();

      for (uint8_t i = 0; i < PARAM_DLG_MAX; i++) {
        ssp.setOffset(dlg->param[i].pos);

        switch (dlg->param[i].type) {

          case PARAM_TYPE_UINT:
            ssp.print(value[i], dlg->param[i].length);
            break;

          case PARAM_TYPE_INT:
            ssp.print(((int8_t)value[i] < 0) ? '-' : ' ');
            ssp.print(abs((int8_t)(value[i])), dlg->param[i].length - 1);
            break;

          case PARAM_TYPE_FLAGS:
            for (uint8_t j = 0, mask = 1; j < dlg->param[i].length; j++, mask <<= 1)
              if (value[i] & mask)  ssp.print(j + 1);
              else                  ssp.print('-');
            break;

          case PARAM_TYPE_LABEL:
            for (uint8_t j = 0; j < dlg->param[i].length; j++)
              ssp.print((dlg->param[i].label)[j]);
            break;

          case PARAM_TYPE_STR:
            char * str = getStringPGM(dlg->param[i].str, value[i]);
            for (uint8_t j = 0; j < dlg->param[i].length; j++)
              ssp.print(str[j]);
            break;
        }
      }

      for (uint8_t i = 0; i < PARAM_DLG_MAX; i++)
        if (dlg->param[i].delim)
          for (uint8_t j = 0; j < dlg->param[i].delim; j++)
            ssp.printDot(dlg->param[i].pos + dlg->param[i].length - 1 + j);

      ssp.blinkOff();
      ssp.startUpdating();

      if (change == CHNG_EDIT) change = CHNG_NO;

      timeoutExit.begin(TIMEOUT_EXIT);
      timeoutBlink.begin(TIMEOUT_BLINK);

      while ((parCurrent < PARAM_DLG_MAX) && (
               (dlg->param[parCurrent].type == PARAM_TYPE_LABEL) ||
               (dlg->param[parCurrent].type == PARAM_TYPE_EMPTY) )) parCurrent++;
    }

    if (timeoutBlink.expired() || (change == CHNG_ENTER)) {
      timeoutBlink.begin(0xFFFF);
      change = CHNG_NO;

      if (dlg->param[parCurrent].type == PARAM_TYPE_FLAGS)
        ssp.blinkDigits(dlg->param[parCurrent].pos + flagCurrent, 1);
      else
        ssp.blinkDigits(dlg->param[parCurrent].pos, dlg->param[parCurrent].length);
    }

    brightDynamicUpdate(value[0]);
    if (timeoutPlay.expired()) {
      timeoutPlay.begin(0xFFFF);
      soundPreplayUpdate(value[0]);
    }

    if (timeoutExit.expired()) {
      ssp.blinkOff();
      return DLG_RESULT_CANCEL;
    }
  }

  ssp.blinkOff();
  return DLG_RESULT_OK;
}

// анимация при входе в меню настроек
void settingsIntro(const char *caption) {
  Timeout timeout;

  ssp.clear();
  ssp.print(getStringPGM(caption, 0));

  if (clockData.getViewEffect() == VIEW_EFFECT_ALL)
    ssp.setEffect(EFFECT_CENTER);
  else
    ssp.setEffect(EFFECT_NONE);

  ssp.startUpdating();

  timeout.begin(TIMEOUT_CAPTION);
  while (!timeout.expired());
}

// анимация при выходе из меню настроек
void settingsOutro() {
  ssp.clear();

  if (clockData.getViewEffect() == VIEW_EFFECT_ALL)
    ssp.setEffect(EFFECT_CENTER_IN);
  else
    ssp.setEffect(EFFECT_NONE);

  ssp.startUpdating();

  delay(500);
}

// получение параметров для установки
void getValues(uint8_t index, uint8_t value[]) {

  switch (index) {

    case 0: // [0] время UTC
      clockData.getTime(&value[0], &value[1], &value[2]);
      break;

    case 1: // [1] дата UTC
      clockData.getDate(&value[0], &value[1], &value[2]);
      break;

    case 2: // [2] часовой пояс
      clockData.getTimeZone(&value[0], &value[1]);
      break;

    case 3: // [3] установки летнего времени
      clockData.getSummerTime(&value[0], &value[1]);
      break;

    case 4: // [4] тихий (ночной) режим - начало
      clockData.getNightTimeBegin(&value[0], &value[1]);
      break;

    case 5: // [5] тихий (ночной) режим - окончание
      clockData.getNightTimeEnd(&value[0], &value[1]);
      break;

    case 6: // [6] яркость часов
      brightDynamicEnable(1);
      value[0] = clockData.getBrightLevel();
      break;

    case 7: // [7] куранты
      clockData.getBell(&value[0], &value[1]);
      break;

    case 8: // [8] звук кнопок
      value[0] = clockData.getBeep();
      break;

    case 9: // [9] стиль отображения часов
      value[0] = clockData.getStyleTime();
      break;

    case 10: // [10] стиль отображения даты
      value[0] = clockData.getStyleDate();
      break;

    case 11: // [11] стиль отображения дня недели
      value[0] = clockData.getWeekDayView();
      break;

    case 12: // [12] стиль отображения дня недели
      value[0] = clockData.getViewEffect();
      break;
  }
}

// сохранение установленных параметров
void setValues(uint8_t index, uint8_t value[]) {

  switch (index) {

    case 0: // [0] время UTC
      clockData.setTime(value[0], value[1], value[2]);
      break;

    case 1: // [1] дата UTC
      clockData.setDate(value[0], value[1], value[2]);
      break;

    case 2: // [2] часовой пояс
      clockData.setTimeZone((int8_t)value[0], (int8_t)value[1]);
      break;

    case 3: // [3] установки летнего времени
      clockData.setSummerTime(value[0], (int8_t)value[1]);
      break;

    case 4: // [4] тихий (ночной) режим - начало
      clockData.setNightTimeBegin(value[0], value[1]);
      break;

    case 5: // [5] тихий (ночной) режим - окончание
      clockData.setNightTimeEnd(value[0], value[1]);
      break;

    case 6: // [6] яркость часов
      brightDynamicEnable(0);
      clockData.setBrightLevel(value[0]);
      break;

    case 7: // [7] куранты
      clockData.setBell(value[0], value[1]);
      break;

    case 8: // [8] звук кнопок
      clockData.setBeep(value[0]);
      break;

    case 9: // [9] стиль отображения часов
      clockData.setStyleTime(value[0]);
      break;

    case 10: // [10] стиль отображения даты
      clockData.setStyleDate(value[0]);
      break;

    case 11: // [11] стиль отображения дня недели
      clockData.setWeekDayView(value[0]);
      break;

    case 12: // [12] стиль отображения дня недели
      clockData.setViewEffect(value[0]);
      break;
  }

  clockData.update();
  utc.update();
  nightTime = clockData.isNightTime(datetime.h, datetime.n);
}

void settMenuChange(uint8_t item, uint8_t change) {
  settItem = getItemStructPGM(&settArr[item - 1]);

  if (clockData.getViewEffect() == VIEW_EFFECT_ALL)
    switch (change) {
      case MENU_CHNG_LEFT:  ssp.setEffect(EFFECT_MOVE_LEFT);  break;
      case MENU_CHNG_RIGHT: ssp.setEffect(EFFECT_MOVE_RIGHT); break;
      case MENU_CHNG_ENTER: ssp.setEffect(EFFECT_CENTER_IN);  break;
    }
  else
    ssp.setEffect(EFFECT_NONE);
  ssp.clear();
  ssp.print(settItem->caption);
  ssp.startUpdating();
}

void settMenuEnter(uint8_t item) {
  uint8_t value[PARAM_DLG_MAX];

  brightLevelArchive();
  getValues(item - 1, value);
  if (dialog(settItem->dialog, value) == DLG_RESULT_OK)
    setValues(item - 1, value);
  else
    brightLevelRestore();
  brightDynamicEnable(0);
}

// =====  УСТАНОВКИ - SEtUP  =====
void settings() {
  Menu menu;

  menu.begin(1, PARAM_ITEMS_MAX);
  menu.onChange(settMenuChange);
  menu.onEnter(settMenuEnter);

  settingsIntro(settSetupCaption);  // анимация при входе
  menu.show();
  settingsOutro();  // анимация при выходе
}


/*********************************************************************/
/*                   МЕНЮ НАСТРОЕК БУДИЛЬНИКОВ                       */
/*********************************************************************/

uint8_t _alarmCurrent;

// получение параметров для установки
void getAlarmValues(uint8_t alarmNo, uint8_t index, uint8_t value[]) {

  switch (index) {
    case 1:     // [1] активность будильника
      value[0] = alarm.isEnabled(alarmNo);
      break;

    case 2:     // [2] время будильника
      value[0] = alarm.getHour(alarmNo);
      value[1] = alarm.getMinute(alarmNo);
      break;

    case 3:     // [3] дни будильника
      value[0] = alarm.getDays(alarmNo);
      break;

    case 4:     // [4] повтор будильника
      value[0] = alarm.getRepeats(alarmNo);
      break;

    case 5:     // [5] период повтора будильника
      value[0] = alarm.getPeriod(alarmNo);
      break;

    case 6:     // [6] мелодия будильника
      value[0] = clockData.getSoundIndex(alarmNo);
      soundPreplay(1);
      break;
  }
}

// сохранение установленных параметров
void setAlarmValues(uint8_t alarmNo, uint8_t index, uint8_t value[]) {

  switch (index) {
    case 1:     // [1] активность будильника
      value[0] ? alarm.enable(alarmNo) : alarm.disable(alarmNo);
      break;

    case 2:     // [2] время будильника
      alarm.setTime(alarmNo, value[0], value[1]);
      break;

    case 3:     // [3] дни будильника
      alarm.setDays(alarmNo, value[0]);
      break;

    case 4:     // [4] повтор будильника
      alarm.setRepeats(alarmNo, value[0]);
      break;

    case 5:     // [5] период повтора будильника
      alarm.setPeriod(alarmNo, value[0]);
      break;

    case 6:     // [6] мелодия будильника
      clockData.setSoundIndex(alarmNo, value[0]);
      break;
  }

  alarm.save(ALARMS_DATA_ADDRESS);
  clockData.update();
}

void alarmParamChange(uint8_t item, uint8_t change) {
  settAlarmItem = getItemStructPGM(&settAlarmArray[item - 1]);

  if (clockData.getViewEffect() == VIEW_EFFECT_ALL)
    switch (change) {
      case MENU_CHNG_LEFT:  ssp.setEffect(EFFECT_MOVE_LEFT);  break;
      case MENU_CHNG_RIGHT: ssp.setEffect(EFFECT_MOVE_RIGHT); break;
      case MENU_CHNG_ENTER: ssp.setEffect(EFFECT_CENTER_IN);  break;
    }
  else
    ssp.setEffect(EFFECT_NONE);
  ssp.clear();
  ssp.print(settAlarmItem->caption);
  ssp.startUpdating();
}

void alarmParamEnter(uint8_t item) {
  uint8_t value[PARAM_DLG_MAX];

  getAlarmValues(_alarmCurrent, item, value);
  if (dialog(settAlarmItem->dialog, value) == DLG_RESULT_OK)
    setAlarmValues(_alarmCurrent, item, value);

  soundPreplay(0);
}

void alarmMenuChange(uint8_t item, uint8_t change) {
  if (clockData.getViewEffect() == VIEW_EFFECT_ALL)
    switch (change) {
      case MENU_CHNG_LEFT:  ssp.setEffect(EFFECT_MOVE_LEFT);  break;
      case MENU_CHNG_RIGHT: ssp.setEffect(EFFECT_MOVE_RIGHT); break;
      case MENU_CHNG_ENTER: ssp.setEffect(EFFECT_CENTER_IN);  break;
    }
  else
    ssp.setEffect(EFFECT_NONE);

  ssp.clear();
  ssp.print(getStringPGM(settAlarmStr, 0));
  ssp.print(item);
  ssp.print(' ');
  if (alarm.isEnabled(item - 1))
    ssp.print('A');
  ssp.startUpdating();
}

void alarmMenuEnter(uint8_t item) {
  _alarmCurrent = item - 1;

  Menu menu;
  menu.begin(1, ALARM_SETT_ITEMS_MAX);
  menu.onChange(alarmParamChange);
  menu.onEnter(alarmParamEnter);

  menu.show();
  settingsOutro();  // анимация при выходе
}

// =====  УСТАНОВКИ БУДИЛЬНИКОВ - ALArnnS  =====
void settingsOfAlarms() {
  Menu menu;
  menu.begin(1, ALARMS_MAX);
  menu.onChange(alarmMenuChange);
  menu.onEnter(alarmMenuEnter);

  settingsIntro(settAlarmCaption);  // анимация при входе
  menu.show();
  settingsOutro();  // анимация при выходе
}
