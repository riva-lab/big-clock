#include "show.h"
#include "common.h"


// режим отображения часов
#define VIEW_MODE_TIME      0           // время
#define VIEW_MODE_DATE      1           // дата
#define VIEW_MODE_WDAY      2           // день недели
#define VIEW_MODE_MIN       0
#define VIEW_MODE_MAX       2

// макросы задания стиля разделителей
#define SHOW_BLINK_DOT(x)   ((1 << 7) + (x << 4))   // 0 - нет, 3 - полоса
#define SHOW_CONST_DOT(x)   ((0 << 7) + (x << 4))   // 0 - нет, 3 - полоса
#define SHOW_POS(x)         (x & 0x0F)              // смещение вывода разделителя

// перевод часов из 24-ч в 12-ч формат
#define SHOW_12H(x)         ((x + 11) % 12 + 1)
#define SHOW_AM_PM(x)       (x < 13) ? 'A' : 'P'
#define SHOW_WEEKDAY(x)     ((x + 6) % 7 + 1)


uint8_t _secondCurrent;
uint8_t _customView = 0;
uint8_t _viewMode = VIEW_MODE_TIME;


void showDots(uint8_t d1, uint8_t d2, uint8_t d3) {
  uint8_t d[3] = { d1, d2, d3 };
  uint8_t dots, blinkDots, pos;
  uint8_t animOff = (clockData.getViewEffect() == VIEW_EFFECT_OFF);

  for (uint8_t i = 0; i < 3; i++) {
    blinkDots   = 0;
    pos         = d[i] & 0x0F;
    dots        = (d[i] >> 4) & 0x07;

    if (dots == 0) continue;
    blinkDots   = (d[i] >> 7) & 0x01;

    if (animOff || !blinkDots || (_secondCurrent & 1))
      switch (dots) {
        case 1: ssp.printDot(pos);                       break;
        case 2: ssp.printDot(pos);  ssp.printDot(++pos); break;
        case 3: ssp.setOffset(pos); ssp.print('-');      break;
      }
  }
}

// вывод времени на индикаторы панели с форматированием по текущему стилю
void showTime(uint8_t h, uint8_t m, uint8_t s, uint8_t w) {
  w = SHOW_WEEKDAY(w);
  _secondCurrent = s;
  ssp.clear();

  switch (clockData.getStyleTime()) {

    //    case 0:             // hh:mm:ss
    default:
      ssp.print(h, 2);
      ssp.print(m, 2);
      ssp.print(s, 2);
      showDots(SHOW_BLINK_DOT(2) + SHOW_POS(1),
               SHOW_BLINK_DOT(2) + SHOW_POS(3),
               0);
      break;

    case 1:             // hh.mm.ss
      ssp.print(h, 2);
      ssp.print(m, 2);
      ssp.print(s, 2);
      showDots(SHOW_BLINK_DOT(1) + SHOW_POS(1),
               SHOW_BLINK_DOT(1) + SHOW_POS(3),
               0);
      break;

    case 2:             // hh:mm:ss.w
      ssp.print(h, 2);
      ssp.print(m, 2);
      ssp.print(s, 2);
      ssp.print(w, 1);
      showDots(SHOW_BLINK_DOT(2) + SHOW_POS(1),
               SHOW_BLINK_DOT(2) + SHOW_POS(3),
               SHOW_CONST_DOT(1) + SHOW_POS(5));
      break;

    case 3:             // hh:mm:ss.a (12-hour)
      ssp.print(SHOW_12H(h), 2);
      ssp.print(m, 2);
      ssp.print(s, 2);
      ssp.print(SHOW_AM_PM(h));
      showDots(SHOW_BLINK_DOT(2) + SHOW_POS(1),
               SHOW_BLINK_DOT(2) + SHOW_POS(3),
               SHOW_CONST_DOT(1) + SHOW_POS(5));
      break;

    case 4:             // hh:mm
      ssp.setOffset(2);
      ssp.print(h, 2);
      ssp.print(m, 2);
      showDots(SHOW_BLINK_DOT(2) + SHOW_POS(3),
               0,
               0);
      break;

    case 5:             // hh.mm
      ssp.setOffset(2);
      ssp.print(h, 2);
      ssp.print(m, 2);
      showDots(SHOW_BLINK_DOT(1) + SHOW_POS(3),
               0,
               0);
      break;

    case 6:             // hh-mm
      ssp.setOffset(1);
      ssp.print(h, 2);
      ssp.setOffset(4);
      ssp.print(m, 2);
      showDots(SHOW_BLINK_DOT(3) + SHOW_POS(3),
               0,
               0);
      break;

    case 7:             // hh:mm w
      ssp.print(h, 2);
      ssp.print(m, 2);
      ssp.setOffset(6);
      ssp.print(w, 1);
      showDots(SHOW_BLINK_DOT(2) + SHOW_POS(1),
               0,
               0);
      break;

    case 8:             // hh.mm w
      ssp.print(h, 2);
      ssp.print(m, 2);
      ssp.setOffset(6);
      ssp.print(w, 1);
      showDots(SHOW_BLINK_DOT(1) + SHOW_POS(1),
               0,
               0);
      break;

    case 9:             // hh-mm w
      ssp.print(h, 2);
      ssp.setOffset(3);
      ssp.print(m, 2);
      ssp.setOffset(6);
      ssp.print(w, 1);
      showDots(SHOW_BLINK_DOT(3) + SHOW_POS(2),
               0,
               0);
      break;

    case 10:            // hh:mm a (12-hour)
      ssp.print(SHOW_12H(h), 2);
      ssp.print(m, 2);
      ssp.setOffset(6);
      ssp.print(SHOW_AM_PM(h));
      showDots(SHOW_BLINK_DOT(2) + SHOW_POS(1),
               0,
               0);
      break;

    case 11:            // hh.mm a (12-hour)
      ssp.print(SHOW_12H(h), 2);
      ssp.print(m, 2);
      ssp.setOffset(6);
      ssp.print(SHOW_AM_PM(h));
      showDots(SHOW_BLINK_DOT(1) + SHOW_POS(1),
               0,
               0);
      break;

    case 12:            // hh-mm a (12-hour)
      ssp.print(SHOW_12H(h), 2);
      ssp.setOffset(3);
      ssp.print(m, 2);
      ssp.setOffset(6);
      ssp.print(SHOW_AM_PM(h));
      showDots(SHOW_BLINK_DOT(3) + SHOW_POS(2),
               0,
               0);
      break;

    case 13:            // a hh:mm (12-hour)
      ssp.print(SHOW_AM_PM(h));
      ssp.setOffset(2);
      ssp.print(SHOW_12H(h), 2);
      ssp.print(m, 2);
      showDots(SHOW_BLINK_DOT(2) + SHOW_POS(3),
               0,
               0);
      break;

    case 14:            // a hh.mm (12-hour)
      ssp.print(SHOW_AM_PM(h));
      ssp.setOffset(2);
      ssp.print(SHOW_12H(h), 2);
      ssp.print(m, 2);
      showDots(SHOW_BLINK_DOT(1) + SHOW_POS(3),
               0,
               0);
      break;

    case 15:            // a hh-mm (12-hour)
      ssp.print(SHOW_AM_PM(h));
      ssp.setOffset(2);
      ssp.print(SHOW_12H(h), 2);
      ssp.setOffset(5);
      ssp.print(m, 2);
      showDots(SHOW_BLINK_DOT(3) + SHOW_POS(4),
               0,
               0);
      break;
  }
}

// вывод даты на индикаторы панели с форматированием по текущему стилю
void showDate(uint8_t y, uint8_t m, uint8_t d, uint8_t w) {
  w = SHOW_WEEKDAY(w);
  ssp.clear();

  switch (clockData.getStyleDate()) {

    //    case 0:             // yy.mm.dd
    default:
      ssp.print(y, 2);
      ssp.print(m, 2);
      ssp.print(d, 2);
      showDots(SHOW_CONST_DOT(1) + SHOW_POS(1),
               SHOW_CONST_DOT(1) + SHOW_POS(3),
               0);
      break;

    case 1:             // yy.mm.dd.w
      ssp.print(y, 2);
      ssp.print(m, 2);
      ssp.print(d, 2);
      ssp.print(w, 1);
      showDots(SHOW_CONST_DOT(1) + SHOW_POS(1),
               SHOW_CONST_DOT(1) + SHOW_POS(3),
               SHOW_CONST_DOT(1) + SHOW_POS(5));
      break;

    case 2:             // dd.mm.yy
      ssp.print(d, 2);
      ssp.print(m, 2);
      ssp.print(y, 2);
      showDots(SHOW_CONST_DOT(1) + SHOW_POS(1),
               SHOW_CONST_DOT(1) + SHOW_POS(3),
               0);
      break;

    case 3:             // mm.dd
      ssp.print(m, 2);
      ssp.print(d, 2);
      showDots(SHOW_CONST_DOT(1) + SHOW_POS(1),
               0,
               0);
      break;

    case 4:             // mm.dd w
      ssp.print(m, 2);
      ssp.print(d, 2);
      ssp.setOffset(6);
      ssp.print(w, 1);
      showDots(SHOW_CONST_DOT(1) + SHOW_POS(1),
               0,
               0);
      break;

    case 5:             // mm-dd
      ssp.setOffset(1);
      ssp.print(m, 2);
      ssp.setOffset(4);
      ssp.print(d, 2);
      showDots(SHOW_CONST_DOT(3) + SHOW_POS(3),
               0,
               0);
      break;

    case 6:             // mm-dd w
      ssp.print(m, 2);
      ssp.setOffset(3);
      ssp.print(d, 2);
      ssp.setOffset(6);
      ssp.print(w, 1);
      showDots(SHOW_CONST_DOT(3) + SHOW_POS(2),
               0,
               0);
      break;

    case 7:             // w mm.dd
      ssp.print(w, 1);
      ssp.setOffset(2);
      ssp.print(m, 2);
      ssp.print(d, 2);
      showDots(SHOW_CONST_DOT(1) + SHOW_POS(3),
               0,
               0);
      break;

    case 8:             // w mm-dd
      ssp.print(w, 1);
      ssp.setOffset(2);
      ssp.print(m, 2);
      ssp.setOffset(5);
      ssp.print(d, 2);
      showDots(SHOW_CONST_DOT(3) + SHOW_POS(4),
               0,
               0);
      break;

    case 9:             // dd.mm
      ssp.setOffset(2);
      ssp.print(d, 2);
      ssp.print(m, 2);
      showDots(SHOW_CONST_DOT(1) + SHOW_POS(3),
               0,
               0);
      break;

    case 10:            // dd.mm w
      ssp.print(d, 2);
      ssp.print(m, 2);
      ssp.setOffset(6);
      ssp.print(w, 1);
      showDots(SHOW_CONST_DOT(1) + SHOW_POS(1),
               0,
               0);
      break;

    case 11:            // dd-mm
      ssp.setOffset(1);
      ssp.print(d, 2);
      ssp.setOffset(4);
      ssp.print(m, 2);
      showDots(SHOW_CONST_DOT(3) + SHOW_POS(3),
               0,
               0);
      break;

    case 12:            // dd-mm w
      ssp.print(d, 2);
      ssp.setOffset(3);
      ssp.print(m, 2);
      ssp.setOffset(6);
      ssp.print(w, 1);
      showDots(SHOW_CONST_DOT(3) + SHOW_POS(2),
               0,
               0);
      break;

    case 13:            // w dd.mm
      ssp.print(w, 1);
      ssp.setOffset(2);
      ssp.print(d, 2);
      ssp.print(m, 2);
      showDots(SHOW_CONST_DOT(1) + SHOW_POS(3),
               0,
               0);
      break;

    case 14:            // w dd-mm
      ssp.print(w, 1);
      ssp.setOffset(2);
      ssp.print(d, 2);
      ssp.setOffset(5);
      ssp.print(m, 2);
      showDots(SHOW_CONST_DOT(3) + SHOW_POS(4),
               0,
               0);
      break;
  }

}

// вывод дня недели на индикаторы панели
void showWeekDay(uint8_t d) {
  d = SHOW_WEEKDAY(d);
  ssp.clear();
  ssp.setOffset(1);
  ssp.print(F("DAY"));
  ssp.setOffset(5);
  ssp.print(d, 1);
}

// вывод часов на индикаторы панели
void showClock() {
  if (datetime.s == 0) showSetModeAuto();

  if (!_customView) {
    if ((datetime.s > 45) && (_viewMode != VIEW_MODE_TIME)) {
      _viewMode = VIEW_MODE_TIME;
    }
    else if ((datetime.s > 41) && (_viewMode != VIEW_MODE_WDAY)) {
      _viewMode = VIEW_MODE_WDAY;
    }
    else if ((datetime.s > 35) && (_viewMode != VIEW_MODE_DATE)) {
      _viewMode = VIEW_MODE_DATE;
    }

    if (!clockData.getWeekDayView() && (_viewMode == VIEW_MODE_WDAY) && (_viewMode != VIEW_MODE_TIME)) {
      _viewMode = VIEW_MODE_TIME;
    }
  } else
    _customView = 1;

  ssp.setEffect(EFFECT_NONE);
  switch (_viewMode) {
    case VIEW_MODE_TIME:
      showTime(datetime.h, datetime.n, datetime.s, datetime.w);
      if (clockData.getViewEffect() == VIEW_EFFECT_ALL)
        ssp.setEffect(EFFECT_SCROLL_UP);
      break;

    case VIEW_MODE_DATE:
      showDate(datetime.y, datetime.m, datetime.d, datetime.w);
      break;

    case VIEW_MODE_WDAY:
      showWeekDay(datetime.w);
      break;
  }

  ssp.startUpdating();
}

void showSetModeAuto() {
  _customView = 0;
}

void showSetModeNext() {
  if (_viewMode == VIEW_MODE_MAX) _viewMode = VIEW_MODE_MIN;
  else _viewMode++;
  _customView = 2;
}

void showSetModePrev() {
  if (_viewMode == VIEW_MODE_MIN) _viewMode = VIEW_MODE_MAX;
  else _viewMode--;
  _customView = 2;
}

uint8_t showIsModeChange() {
  return (_customView == 2);
}
