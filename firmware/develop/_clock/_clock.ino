#include "common.h"
#include "settings.h"
#include "show.h"


// инициализация системы
void setup() {  
  Serial.begin(115200);
  Serial.print(F("*** Big7digitsClock Start.\n"));

  commonInit();  
}


// рабочий цикл
void loop() {
  static t_datetime timeCurrent;
  uint8_t btnPressedKey = btn.getPressed();
  uint8_t btnClickedKey = btn.getClicked();

  // выключение будильника нажатием кнопок
  if (alarmWork && (btnPressedKey || btnClickedKey)) {
    alarmWork = 0;
    alarm.hushAll();
    if (player.isPlaying()) {
      player.stop();
      btnPressedKey = BUTTON_NONE;
      btnClickedKey = BUTTON_NONE;
    }
  }

  //  speedTest();

  // обработка длинного нажатия кнопки
  switch (btnPressedKey) {

    // кнопка ВВОД - переход к настройкам часов
    case BUTTON_MD:
      settings();
      showSetModeAuto();
      break;

    // кнопка ВВЕРХ - переход к настройкам будильников
    case BUTTON_UP:
      settingsOfAlarms();
      showSetModeAuto();
      break;
  }

  // обработка короткого нажатия кнопки
  switch (btnClickedKey) {

    // кнопка ВВЕРХ - ручное переключение между отображаемыми данными
    case BUTTON_UP:
      showSetModePrev();
      break;

    // кнопка ВНИЗ - ручное переключение между отображаемыми данными
    case BUTTON_DN:
      showSetModeNext();
      break;
  }

  static uint32_t clockUpdTime;
  if ((uint16_t)(millis() - clockUpdTime) > 100) {
    clockUpdTime = millis();
    utc.update();

    // задержка необходима во избежание повторного входа в эту ветку,
    // поскольку время выполнения всего цикла меньше 1 мс
    delay(1);

    if ((timeCurrent != utc.getTime()) || showIsModeChange()) {
      timeCurrent   = utc.getTime();

      datetime.y    = utc.getYear() - 2000;
      datetime.m    = utc.getMonth();
      datetime.d    = utc.getDay();
      datetime.h    = utc.getHour();
      datetime.n    = utc.getMinute();
      datetime.s    = utc.getSecond();
      datetime.w    = utc.getDayOfWeek();
      zone          = utc.getZone() / 60;
      zonem         = utc.getZone() % 60;
      summer_t      = utc.getSummerTime();

      nightTime = clockData.isNightTime(datetime.h, datetime.n); // флаг "тихое время"
      showClock(); // вывод часов на дисплей

      //    printDateDebug();
    }

    // авторегулирование яркости панели
    if (clockData.getBrightLevel() == BRIGHT_AUTO)
      autoBrightnessCheck();
  }

  static uint32_t alarmUpdTime;
  if ((uint16_t)(millis() - alarmUpdTime) > 1000) {
    alarmUpdTime = millis();

    // проверка срабатывания будильников
    alarm.update();

    if (alarm.activated()) {
      for (uint8_t i = 0; i < ALARMS_MAX; i++)
        if (alarm.checkOutput(i) && !player.isPlaying()) {
          alarmWork = 1;
          player.load(GET_MELODY_FROM_LIST(clockData.getSoundIndex(i) - 1));
          player.play();
          break;
        }
    }

    // проверка и действие курантов
    bellsCheck(datetime.h, datetime.n);
  }
}


void printNumber2(uint8_t x) {
  if (x < 10) Serial.print(0);
  Serial.print(x);
}

void printDateDebug() {
  printNumber2(datetime.h);
  Serial.print(':');
  printNumber2(datetime.n);
  Serial.print(':');
  printNumber2(datetime.s);
  Serial.print(' ');

  Serial.print(datetime.y);
  Serial.print('.');
  printNumber2(datetime.m);
  Serial.print('.');
  printNumber2(datetime.d);
  Serial.print(' ');

  Serial.print(F(" day "));
  Serial.print(datetime.w);

  Serial.print(F(" UTC"));
  Serial.print(((zone >= 0) && (zonem >= 0)) ? "+" : "-");
  Serial.print(abs(zone));
  Serial.print((abs(zonem) < 10) ? ".0" : ".");
  Serial.print(abs(zonem));

  Serial.print(F(" DST:"));
  Serial.print((summer_t > 0) ? "+1h" : "0h");

  Serial.println();
}

void speedTest() {
  static uint32_t t, tp;
  tp = t;
  t = millis();
  //  Serial.print(F(" loop time = "));
  Serial.print(t - tp);
  //  Serial.print(F(" ms"));
  Serial.println();
}
