#include "buttons.h"
#include "common.h"
#include <fastIO.h>


void Buttons::begin() {
  for (uint8_t i = 0; i < BUTTONS_COUNT; i++)
    fastPinMode(buttonsPin[i], INPUT_PULLUP);
}

void Buttons::reset() {
  _actionCode = BUTTON_ACTION_NO;
  for (uint8_t i = 0; i < BUTTONS_COUNT; i++)
    button[i].status = 0;
}

void Buttons::update() {
  uint16_t ms = (uint16_t)(millis());
  uint16_t delta;

  for (uint8_t i = 0; i < BUTTONS_COUNT; i++) {

    button[i].activated = (fastDigitalRead(buttonsPin[i]) == 0);

    if (button[i].activated) {

      if (!button[i].activatedPrev)
        button[i].time = ms;

      delta = (ms - button[i].time) & 0x7FFF;

      if (!button[i].clickFlag && (delta > BUTTON_CLICK_TIME) && (delta < BUTTON_PRESS_TIME))
        button[i].clickFlag = 1;

      if (!button[i].pressFlag && (delta > BUTTON_PRESS_TIME)) {
        button[i].clickFlag = 0;
        button[i].pressFlag = 1;
        button[i].press     = 1;
        button[i].hold      = 1;
        _actionCode         = BUTTON_ACTION_PRESS;
      }
    } else {
      if (button[i].clickFlag) {
        button[i].click     = 1;
        _actionCode         = BUTTON_ACTION_CLICK;
      }

      button[i].pressFlag   = 0;
      button[i].clickFlag   = 0;
      button[i].hold        = 0;
    }

    button[i].activatedPrev = button[i].activated;
  }
}


void Buttons::click(uint8_t buttonCode) {
  for (uint8_t i = 0; i < BUTTONS_COUNT; i++, buttonCode >>= 1)
    if (buttonCode & 1)
      button[BUTTONS_COUNT - 1 - i].click = 1;
}

void Buttons::press(uint8_t buttonCode) {
  for (uint8_t i = 0; i < BUTTONS_COUNT; i++, buttonCode >>= 1)
    if (buttonCode & 1)
      button[BUTTONS_COUNT - 1 - i].press = 1;
}


uint8_t Buttons::getClicked() {
  uint8_t result = 0;

  for (uint8_t i = 0; i < BUTTONS_COUNT; i++) {
    result <<= 1;
    if (button[i].click) result++;
    button[i].click = 0;
  }

  return result;
}

uint8_t Buttons::getPressed() {
  uint8_t result = 0;

  for (uint8_t i = 0; i < BUTTONS_COUNT; i++) {
    result <<= 1;
    if (button[i].press) result++;
    button[i].press = 0;
  }

  return result;
}

uint8_t Buttons::getHolding() {
  uint8_t result = 0;

  for (uint8_t i = 0; i < BUTTONS_COUNT; i++) {
    result <<= 1;
    if (button[i].hold) result++;
  }

  return result;
}

uint8_t Buttons::getAction() {
  uint8_t result    = _actionCode;
  _actionCode       = BUTTON_ACTION_NO;

  return result;
}
