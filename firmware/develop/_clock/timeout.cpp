#include "timeout.h"


Timeout::Timeout() {
}

Timeout::Timeout(uint16_t timeoutMillis) {
  begin(timeoutMillis);
}

void Timeout::begin(uint16_t timeoutMillis) {
  _timeStart    = (uint16_t)(millis());
  _timeout      = timeoutMillis;
}

uint8_t Timeout::expired() {
  return (uint16_t)(millis() - _timeStart) > _timeout;
}
