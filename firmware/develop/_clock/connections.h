#ifndef CONNECTIONS_H
#define CONNECTIONS_H


// подключение модуля RTC по I2C использует выводы A4-SDA и A5-SCL

// подключение семисегментной панели к Arduino Atmega328P
#define PIN_CLK         8
#define PIN_DATA        5
#define PIN_STR         7
#define PIN_PWM         6


// подключение кнопок к Arduino Atmega328P
#define PIN_BTN_UP      A1
#define PIN_BTN_MD      A2
#define PIN_BTN_DN      A3

// подключение фоторезистора к Arduino Atmega328P
#define PIN_PHOTO_R     A0

// подключение динамика к Arduino Atmega328P
#define PIN_SPEAKER_A   9
#define PIN_SPEAKER_B   10


#endif /* CONNECTIONS_H */
