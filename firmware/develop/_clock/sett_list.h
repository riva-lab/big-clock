#ifndef SETTINGSLIST_H
#define SETTINGSLIST_H

#include "sett_item.h"
#include "data.h"


#define PARAM_ITEMS_MAX     13  // количество настроек (пунктов меню)
#define STYLES_OF_TIME      16  // количество стилей отображения времени
#define STYLES_OF_DATE      15  // количество стилей отображения даты


/*********************************************************************/
/*                          Строковые наборы                         */
/*********************************************************************/

// список для параметра-переключателя
const char strDstMode[][PARAM_TXT_SIZE] PROGMEM = {
  "OFF ",
  "ON  ",
  "AUTO"
};

// список стилей отображения времени
const char strStyleTime[STYLES_OF_TIME][PARAM_TXT_SIZE] PROGMEM = {
  "HH.N.N.S.S  ",
  "HH.NN.SS    ",
  "HH.N.N.S.S.D",
  "HH.N.N.S.S.A",
  "HH.N.N      ",
  "HH.NN       ",
  "HH-NN       ",
  "HH.N.N D    ",
  "HH.NN D     ",
  "HH-NN D     ",
  "HH.N.N A    ",
  "HH.NN A     ",
  "HH-NN A     ",
  "A HH.N.N    ",
  "A HH.NN     ",
  "A HH-NN     "
};

// список стилей отображения даты
const char strStyleDate[STYLES_OF_DATE][PARAM_TXT_SIZE] PROGMEM = {
  "YY.NN.DD    ",
  "YY.NN.DD.D  ",
  "DD.NN.YY    ",
  "NN.DD       ",
  "NN.DD D     ",
  "NN-DD       ",
  "NN-DD D     ",
  "D NN.DD     ",
  "D NN-DD     ",
  "DD.NN       ",
  "DD.NN D     ",
  "DD-NN       ",
  "DD-NN D     ",
  "D DD.NN     ",
  "D DD-NN     "
};

// список для параметра "эффекты отображения"
const char strEffects[][PARAM_TXT_SIZE] PROGMEM = {
  "OFF ",
  "DOTS",
  "ALL "
};


/*********************************************************************/
/*********************************************************************/
/*                         МЕНЮ НАСТРОЕК ЧАСОВ                       */
/*********************************************************************/
/*********************************************************************/

const char settSetupCaption[] PROGMEM = " SETUP";

/*********************************************************************/
/*                     ДИАЛОГИ НАСТРОЕК ЧАСОВ                        */
/*********************************************************************/

// диалог - "Установка времени"
const dialog_t dlgSetTime PROGMEM = {
  {
    {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      23,               // макс.значение
      0,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }, {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      59,               // макс.значение
      2,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }, {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      59,               // макс.значение
      4,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установка даты"
const dialog_t dlgSetDate PROGMEM = {
  {
    {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      99,               // макс.значение
      0,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT1  // разделитель
    }, {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      1,                // мин.значение
      12,               // макс.значение
      2,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT1  // разделитель
    }, {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      1,                // мин.значение
      31,               // макс.значение
      4,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установка часового пояса"
const dialog_t dlgSetZone PROGMEM = {
  {
    {
      PARAM_TYPE_INT,   // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      -12,              // мин.значение
      14,               // макс.значение
      0,                // позиция вывода параметра
      3,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_INT,   // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      -59,              // мин.значение
      59,               // макс.значение
      4,                // позиция вывода параметра
      3,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_LABEL, // тип
      "H  ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      3,                // позиция вывода параметра
      1,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }
  }
};

// диалог - "Установка летнего времени"
const dialog_t dlgSetSummerTime PROGMEM = {
  {
    {
      PARAM_TYPE_STR,   // тип
      "   ",            // строка (для текстового параметра)
      strDstMode[0],    // указатель на начало массива строк
      0,                // мин.значение
      2,                // макс.значение
      0,                // позиция вывода параметра
      4,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT1  // разделитель
    }, {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      120,              // макс.значение
      4,                // позиция вывода параметра
      3,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установка времени тихого режима, начало"
const dialog_t dlgSetNightTimeBegin PROGMEM = {
  {
    {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      23,               // макс.значение
      2,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }, {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      59,               // макс.значение
      4,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_LABEL, // тип
      "NB ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }
  }
};

// диалог - "Установка времени тихого режима, конец"
const dialog_t dlgSetNightTimeEnd PROGMEM = {
  {
    {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      23,               // макс.значение
      2,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }, {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      59,               // макс.значение
      4,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_LABEL, // тип
      "NE ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }
  }
};

// диалог - "Установка яркости панели"
const dialog_t dlgSetBright PROGMEM = {
  {
    {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      BRIGHT_MAX,       // макс.значение
      3,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_LABEL, // тип
      "LT ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установки курантов"
const dialog_t dlgSetBells PROGMEM = {
  {
    {
      PARAM_TYPE_STR,   // тип
      "   ",            // строка (для текстового параметра)
      strDstMode[0],    // указатель на начало массива строк
      0,                // мин.значение
      1,                // макс.значение
      0,                // позиция вывода параметра
      4,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT1  // разделитель
    }, {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      1,                // мин.значение
      240,              // макс.значение
      4,                // позиция вывода параметра
      3,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установки звука кнопок"
const dialog_t dlgSetBeeps PROGMEM = {
  {
    {
      PARAM_TYPE_STR,   // тип
      "   ",            // строка (для текстового параметра)
      strDstMode[0],    // указатель на начало массива строк
      0,                // мин.значение
      1,                // макс.значение
      3,                // позиция вывода параметра
      4,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_LABEL, // тип
      "BP ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установка стиля отображения часов"
const dialog_t dlgSetStyleTime PROGMEM = {
  {
    {
      PARAM_TYPE_STR,   // тип
      "   ",            // строка (для текстового параметра)
      strStyleTime[0],  // указатель на начало массива строк
      0,                // мин.значение
      STYLES_OF_DATE - 1, // макс.значение
      0,                // позиция вывода параметра
      PARAM_TXT_SIZE,   // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установка стиля отображения даты"
const dialog_t dlgSetStyleDate PROGMEM = {
  {
    {
      PARAM_TYPE_STR,   // тип
      "   ",            // строка (для текстового параметра)
      strStyleDate[0],  // указатель на начало массива строк
      0,                // мин.значение
      STYLES_OF_DATE - 1, // макс.значение
      0,                // позиция вывода параметра
      PARAM_TXT_SIZE,   // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установка отображения дня недели"
const dialog_t dlgSetWeekDay PROGMEM = {
  {
    {
      PARAM_TYPE_STR,   // тип
      "   ",            // строка (для текстового параметра)
      strDstMode[0],    // указатель на начало массива строк
      0,                // мин.значение
      1,                // макс.значение
      3,                // позиция вывода параметра
      3,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_LABEL, // тип
      "D   ",           // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      1,                // макс.значение
      1,                // позиция вывода параметра
      1,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установка эффектов отображения"
const dialog_t dlgSetEffect PROGMEM = {
  {
    {
      PARAM_TYPE_STR,   // тип
      "   ",            // строка (для текстового параметра)
      strEffects[0],    // указатель на начало массива строк
      0,                // мин.значение
      2,                // макс.значение
      2,                // позиция вывода параметра
      4,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_LABEL, // тип
      "EF  ",           // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};


/*********************************************************************/
/*                           МЕНЮ НАСТРОЕК                           */
/*********************************************************************/

// массив настроек (в порядке меню)
const settingItem_t settArr[PARAM_ITEMS_MAX] PROGMEM = {
  {
    "UTC T",      // [0] время UTC
    &dlgSetTime
  },
  {
    "UTC D",      // [1] дата UTC
    &dlgSetDate
  },
  {
    "ZONE",       // [2] часовой пояс
    &dlgSetZone
  },
  {
    "DST",        // [3] установки летнего времени
    &dlgSetSummerTime
  },
  {
    "NIGHT B",    // [4] тихий (ночной) режим - начало
    &dlgSetNightTimeBegin
  },
  {
    "NIGHT E",    // [5] тихий (ночной) режим - окончание
    &dlgSetNightTimeEnd
  },
  {
    "LIGHT",      // [6] яркость часов
    &dlgSetBright
  },
  {
    "BELLS",      // [7] куранты
    &dlgSetBells
  },
  {
    "BEEPS",      // [8] звук кнопок
    &dlgSetBeeps
  },
  {
    "STYLE T",    // [9] стиль отображения часов
    &dlgSetStyleTime
  },
  {
    "STYLE D",    // [10] стиль отображения даты
    &dlgSetStyleDate
  },
  {
    "DAY",        // [11] стиль отображения дня недели
    &dlgSetWeekDay
  },
  {
    "EFFECTS",    // [12] эффекты отображения
    &dlgSetEffect
  }
};


/*********************************************************************/
/*********************************************************************/
/*                    МЕНЮ НАСТРОЕК БУДИЛЬНИКОВ                      */
/*********************************************************************/
/*********************************************************************/

#define ALARM_SETT_ITEMS_MAX    6  // количество настроек будильника

const char settAlarmCaption[] PROGMEM = "ALARNNS";
const char settAlarmStr[]     PROGMEM = "ALA-";

/*********************************************************************/
/*                   ДИАЛОГИ НАСТРОЕК БУДИЛЬНИКОВ                    */
/*********************************************************************/

// диалог - "Установка активности будильника"
const dialog_t dlgAlarmEnable PROGMEM = {
  {
    {
      PARAM_TYPE_STR,   // тип
      "   ",            // строка (для текстового параметра)
      strDstMode[0],    // указатель на начало массива строк
      0,                // мин.значение
      1,                // макс.значение
      3,                // позиция вывода параметра
      3,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_LABEL, // тип
      "EN  ",           // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установка времени будильника"
const dialog_t dlgAlarmTime PROGMEM = {
  {
    {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      23,               // макс.значение
      2,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }, {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      59,               // макс.значение
      4,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_LABEL, // тип
      "T  ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установка дней будильника"
const dialog_t dlgAlarmDays PROGMEM = {
  {
    {
      PARAM_TYPE_FLAGS, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      6,                // макс.значение
      0,                // позиция вывода параметра
      7,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установка повторов будильника"
const dialog_t dlgAlarmRepeats PROGMEM = {
  {
    {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      99,               // макс.значение
      4,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_LABEL, // тип
      "REP",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      1,                // позиция вывода параметра
      3,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установка периода повтора будильника"
const dialog_t dlgAlarmPeriod PROGMEM = {
  {
    {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      1,                // мин.значение
      99,               // макс.значение
      4,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_LABEL, // тип
      "PER",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      1,                // позиция вывода параметра
      3,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

// диалог - "Установка мелодии будильника"
const dialog_t dlgAlarmSound PROGMEM = {
  {
    {
      PARAM_TYPE_UINT,  // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      1,                // мин.значение
      USER_MELODIES_SAMPLES, // макс.значение
      3,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }, {
      PARAM_TYPE_LABEL, // тип
      "SN",             // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      2,                // ограничение длины вывода параметра
      PARAM_DELIM_DOT2  // разделитель
    }, {
      PARAM_TYPE_EMPTY, // тип
      "   ",            // строка (для текстового параметра)
      NULL,             // указатель на начало массива строк
      0,                // мин.значение
      0,                // макс.значение
      0,                // позиция вывода параметра
      0,                // ограничение длины вывода параметра
      PARAM_DELIM_NO    // разделитель
    }
  }
};

/*********************************************************************/
/*                    МЕНЮ НАСТРОЕК БУДИЛЬНИКОВ                      */
/*********************************************************************/

// массив настроек (в порядке меню)
const settingItem_t settAlarmArray[ALARM_SETT_ITEMS_MAX] PROGMEM = {
  {
    "ENABLE",       // [1] активность будильника
    &dlgAlarmEnable
  },
  {
    "TINNE",        // [2] время будильника
    &dlgAlarmTime
  },
  {
    "DAYS",         // [3] дни будильника
    &dlgAlarmDays
  },
  {
    "REPEATS",      // [4] повтор будильника
    &dlgAlarmRepeats
  },
  {
    "PERIOD",       // [5] период повтора будильника
    &dlgAlarmPeriod
  },
  {
    "SOUND",        // [6] мелодия будильника
    &dlgAlarmSound
  }
};

#endif /* SETTINGSLIST_H */
