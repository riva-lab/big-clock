#ifndef TIMEOUT_H
#define TIMEOUT_H

#include <stdint.h>

#if defined(ARDUINO) && (ARDUINO >= 100)
#include "Arduino.h"
#else
#include "WProgram.h"
#include <pins_arduino.h>
#endif


class Timeout {

  public:
    Timeout();
    Timeout(uint16_t timeoutMillis);
    
    void    begin(uint16_t timeoutMillis);
    uint8_t expired();

  private:
    uint16_t _timeStart;
    uint16_t _timeout;
};

#endif /* TIMEOUT_H */
